import warnings

from django.views.generic.dates import ArchiveIndexView, DateDetailView, DayArchiveView, MonthArchiveView, YearArchiveView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.base import RedirectView
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from .models import Album, Photo
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from .forms import AlbumForm, PhotoForm




class PhotoAdd(CreateView):
    model = Photo
    fields = ['album', 'image', 'title', 'description']

class PhotoUpdate(UpdateView):
    model = Photo
    fields = ['album', 'image', 'title', 'description']
    template_name = 'photo_update_form.html'

class PhotoDelete(DeleteView):
    model = Photo
    success_url = reverse_lazy('gallery:index')


@login_required
def album_new(request):
    if request.method == "POST":
        form = AlbumForm(request.POST)
        if form.is_valid():
            album = form.save(commit=False)
            album.author = request.user
            album.save()
            return redirect('album-detail', pk=album.pk)
    else:
        form = AlbumForm()
    return render(request, 'gallery/gallery_edit.html', {'form': form})


class AlbumUpdate(UpdateView):
    model = Album
    fields = ['user', 'title', 'description']
    template_name_suffix = '_update_form'  

class AlbumDelete(DeleteView):
    model = Album
    success_url = reverse_lazy('index')


def index(request):
    context = {
        'title': 'Home page title!!!'
    }
    return render(request, 'gallery/index.html', context)

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/accounts/login/')
    else:
        form = UserCreationForm()

        args = {'form': form}
        return render(request, 'registration/reg_form.html', args)





class AlbumListView(ListView):
    queryset = Album.objects.filter(is_public=True)
    paginate_by = 20


class AlbumDetailView(DetailView):
    queryset = Album.objects.filter(is_public=True)


class AlbumDateView(object):
    queryset = Album.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_empty = True


class AlbumDateDetailView(AlbumDateView, DateDetailView):
    pass


class AlbumArchiveIndexView(AlbumDateView, ArchiveIndexView):
    pass


class AlbumDayArchiveView(AlbumDateView, DayArchiveView):
    pass


class AlbumMonthArchiveView(AlbumDateView, MonthArchiveView):
    pass


class AlbumYearArchiveView(AlbumDateView, YearArchiveView):
    make_object_list = True

# Photo views.


class PhotoListView(ListView):
    queryset = Photo.objects.filter(is_public=True)
    paginate_by = 20


class PhotoDetailView(DetailView):
    queryset = Photo.objects.filter(is_public=True)


class PhotoDateView(object):
    queryset = Photo.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_empty = True


class PhotoDateDetailView(PhotoDateView, DateDetailView):
    pass


class PhotoArchiveIndexView(PhotoDateView, ArchiveIndexView):
    pass


class PhotoDayArchiveView(PhotoDateView, DayArchiveView):
    pass


class PhotoMonthArchiveView(PhotoDateView, MonthArchiveView):
    pass


class PhotoYearArchiveView(PhotoDateView, YearArchiveView):
    make_object_list = True


# Deprecated views.

class DeprecatedMonthMixin(object):

    """Representation of months in urls has changed from a alpha representation ('jan' for January)
    to a numeric representation ('01' for January).
    Properly deprecate the previous urls."""

    query_string = True

    month_names = {'jan': '01',
                   'feb': '02',
                   'mar': '03',
                   'apr': '04',
                   'may': '05',
                   'jun': '06',
                   'jul': '07',
                   'aug': '08',
                   'sep': '09',
                   'oct': '10',
                   'nov': '11',
                   'dec': '12', }

    def get_redirect_url(self, *args, **kwargs):
        print('a')
        warnings.warn(
            DeprecationWarning('Months are now represented in urls by numbers rather than by '
                               'their first 3 letters. The old style will be removed in Photologue 3.4.'))


class AlbumDateDetailOldView(DeprecatedMonthMixin, RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        super(AlbumDateDetailOldView, self).get_redirect_url(*args, **kwargs)
        return reverse('gallery:album-detail', kwargs={'year': kwargs['year'],
                                                            'month': self.month_names[kwargs['month']],
                                                            'day': kwargs['day']})


class AlbumDayArchiveOldView(DeprecatedMonthMixin, RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        super(AlbumDayArchiveOldView, self).get_redirect_url(*args, **kwargs)
        return reverse('gallery:album-archive-day', kwargs={'year': kwargs['year'],
                                                                 'month': self.month_names[kwargs['month']],
                                                                 'day': kwargs['day']})


class AlbumMonthArchiveOldView(DeprecatedMonthMixin, RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        super(AlbumMonthArchiveOldView, self).get_redirect_url(*args, **kwargs)
        return reverse('gallery:album-archive-month', kwargs={'year': kwargs['year'],
                                                                   'month': self.month_names[kwargs['month']]})


class PhotoDateDetailOldView(DeprecatedMonthMixin, RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        super(PhotoDateDetailOldView, self).get_redirect_url(*args, **kwargs)
        return reverse('gallery:photo-detail', kwargs={'year': kwargs['year'],
                                                          'month': self.month_names[kwargs['month']],
                                                          'day': kwargs['day']})


class PhotoDayArchiveOldView(DeprecatedMonthMixin, RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        super(PhotoDayArchiveOldView, self).get_redirect_url(*args, **kwargs)
        return reverse('gallery:photo-archive-day', kwargs={'year': kwargs['year'],
                                                               'month': self.month_names[kwargs['month']],
                                                               'day': kwargs['day']})


class PhotoMonthArchiveOldView(DeprecatedMonthMixin, RedirectView):
    permanent = True

    def get_redirect_url(self, *args, **kwargs):
        super(PhotoMonthArchiveOldView, self).get_redirect_url(*args, **kwargs)
        return reverse('gallery:photo-archive-month', kwargs={'year': kwargs['year'],
                                                                 'month': self.month_names[kwargs['month']]})

