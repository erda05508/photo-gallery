from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
from django.core.urlresolvers import reverse
from django.conf import settings
from .managers import AlbumQuerySet, PhotoQuerySet

LATEST_LIMIT = getattr(settings, 'GALLERY_ALBUM_LATEST_LIMIT', None)
SAMPLE_SIZE = getattr(settings, 'GALLERY_ALBUM_SAMPLE_SIZE', 5)


class Album(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=200, blank=False, verbose_name='Названия')
    description = models.TextField(verbose_name='Описания')
    created_d = models.DateTimeField(default=now)
    mod_d = models.DateTimeField(auto_now_add=False, auto_now=True)
    is_public = models.BooleanField(default=True)
    objects = AlbumQuerySet.as_manager()

    class Meta:
        ordering = ["mod_d"]

    def __str__(self):
        return "Альбом: %s, User: %s" % (self.title, self.user)
   
    def get_absolute_url(self):
        return reverse('gallery:pl-gallery', kwargs={'pk': self.pk})
 

class Photo(models.Model):
    album = models.ForeignKey(Album)
    title = models.CharField(max_length=200, blank=False)
    image = models.ImageField(upload_to='mediafiles')
    created_d = models.DateTimeField(default=now)
    mod_d = models.DateTimeField(auto_now_add=False, auto_now=True)
    description = models.CharField(max_length=200, blank=True)
    is_public = models.BooleanField(default=True)
    objects = PhotoQuerySet.as_manager()

    class Meta:
        ordering = ["mod_d"]
  
    def __str__(self):
        return "%s, %s" % (self.title, self.album)
 
    def save(self, *args, **kwargs):
        if self.pk is None:
            self.pk = self.id
        super(Photo, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('gallery:pl-photo', args=[self.id])

    def public_albums(self):
        """Return the public galleries to which this photo belongs."""
        return self.albums.filter(is_public=True)
