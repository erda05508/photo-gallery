from django.conf.urls import url
from . import views
from django.contrib.auth.views import login, logout
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', login, {'template_name': 'registration/login.html'}),
    url(r'^logout/$', logout, {'template_name': 'registration/logged_out.html'}),
    url(r'^register/$', views.register, name='register'),
    url(r'^album/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/(?P<pk>[0-9]+)/$', views.AlbumDateDetailView.as_view(month_format='%m'), name='gallery-detail'),
    url(r'^album/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/$', views.AlbumDayArchiveView.as_view(month_format='%m'), name='gallery-archive-day'),
    url(r'^album/(?P<year>\d{4})/(?P<month>[0-9]{2})/$', views.AlbumMonthArchiveView.as_view(month_format='%m'), name='gallery-archive-month'),
    url(r'^album/(?P<year>\d{4})/$', views.AlbumYearArchiveView.as_view(), name='pl-gallery-archive-year'),
    url(r'^album/$', views.AlbumArchiveIndexView.as_view(), name='pl-gallery-archive'),
    url(r'^album/(?P<pk>[0-9]+)/$', views.AlbumDetailView.as_view(), name='pl-gallery'),
    url(r'^albumlist/$', views.AlbumListView.as_view(), name='album-list'), 
    url(r'^album/new/$', views.album_new, name='album_new'),
    url(r'^album/(?P<pk>[0-9]+)/$', views.AlbumUpdate.as_view(), name='album_update'),
    url(r'^album/(?P<pk>[0-9]+)/delete/$', views.AlbumDelete.as_view(), name='album_delete'),
    
    url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/(?P<pk>[0-9]+)/$', views.PhotoDateDetailView.as_view(month_format='%m'), name='photo-detail'),
    url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/$', views.PhotoDayArchiveView.as_view(month_format='%m'), name='photo-archive-day'),
    url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/$', views.PhotoMonthArchiveView.as_view(month_format='%m'), name='photo-archive-month'),
    url(r'^photo/(?P<year>\d{4})/$', views.PhotoYearArchiveView.as_view(), name='pl-photo-archive-year'),
    url(r'^photo/$', views.PhotoArchiveIndexView.as_view(), name='pl-photo-archive'),
    url(r'^photo/(?P<pk>[0-9]+)/$', views.PhotoDetailView.as_view(), name='pl-photo'),
    url(r'^photolist/$', views.PhotoListView.as_view(), name='photo-list'),
    url(r'^photo/new/$', views.PhotoAdd.as_view(), name='photo_new'),
    url(r'^photo/change/(?P<pk>[0-9]+)/$', views.PhotoUpdate.as_view(), name='photo_change'),
    url(r'^photo/delete/(?P<pk>[0-9]+)/$', views.PhotoDelete.as_view(), name='photo_delete'),

    # Deprecated URLs.
    url(r'^album/(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\w{1,2})/(?P<pk>[0-9]+)/$', views.AlbumDateDetailOldView.as_view(), name='pl-gallery-detail'),
    url(r'^album/(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\w{1,2})/$', views.AlbumDayArchiveOldView.as_view(), name='pl-gallery-archive-day'),
    url(r'^album/(?P<year>\d{4})/(?P<month>[a-z]{3})/$', views.AlbumMonthArchiveOldView.as_view(), name='pl-gallery-archive-month'),
    url(r'^photo/(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\w{1,2})/(?P<pk>[0-9]+)/$', views.PhotoDateDetailOldView.as_view(), name='pl-photo-detail'),
    url(r'^photo/(?P<year>\d{4})/(?P<month>[a-z]{3})/(?P<day>\w{1,2})/$', views.PhotoDayArchiveOldView.as_view(), name='pl-photo-archive-day'),
    url(r'^photo/(?P<year>\d{4})/(?P<month>[a-z]{3})/$', views.PhotoMonthArchiveOldView.as_view(), name='pl-photo-archive-month')
]
