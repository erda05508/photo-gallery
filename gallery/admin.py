from django.contrib import admin
from .models import Album, Photo

class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 0

class AlbumAdmin(admin.ModelAdmin):
    list_display = ['title', 'user', 'description']
    list_filter = ['title', 'user']
    inlines = [PhotoInline]
 
    class Meta:
        model = Album

admin.site.register(Album, AlbumAdmin)

class PhotoAdmin(admin.ModelAdmin):
    list_display = ['title', 'album', 'image', 'is_public']
    list_filter = ['title', 'album']

    class Meta:
        model = Photo

admin.site.register(Photo, PhotoAdmin)

