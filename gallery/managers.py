from django.db.models.query import QuerySet
from django.conf import settings


class SharedQueries(object):

    def is_public(self):
        return self.filter(is_public=True)

    def on_site(self):
        return self.filter(sites__id=settings.SITE_ID)


class AlbumQuerySet(SharedQueries, QuerySet):
    pass


class PhotoQuerySet(SharedQueries, QuerySet):
    pass
